﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace SC.BL.Domain
{
    public class User : IdentityUser
    {
        //here you can add your own property 
        //public string Gender { get; set; }

        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}