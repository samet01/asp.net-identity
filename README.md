# Project

* Groepscode: INF201-10
* Studenten: Michiels Jaimy & Gündogan Samet
* Topic: ASP.NET Identity

### Features:
* User login - local or extern account
* User registration
* User change password
* Secure - cookie
* Secure - role admin
* Add role to user
* Admin role - can delete user 
* User can own tickets

### Todo's:
* User - Seed method
* secure - jwt