using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Proxies; // NuGet-package!
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Debug; // NuGet-package!
using SC.BL.Domain;

namespace SC.DAL.EF
{
    //IdentityDbContext is a special database for identity
    //DbContext is a normal database
    public class SupportCenterDbContext : IdentityDbContext<User>
    {
        public SupportCenterDbContext()
        {
            SupportCenterDbInitializer.Initialize(this, dropCreateDatabase: false);
        }

        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<HardwareTicket> HardwareTickets { get; set; }
        public DbSet<TicketResponse> TicketResponses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Data Source=..\\..\\SupportCenterDb_EFCodeFirst.db");

            // configure logging-information
            optionsBuilder.UseLoggerFactory(LoggerFactory.Create(
                builder => builder.AddDebug()
            ));

            // configure lazy-loading: requires ALL navigation-properties to be 'virtual'!!
            //optionsBuilder.UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //You need write the below -> otherwise InvalidOperationException
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Ticket>().HasIndex(t => t.State);

            modelBuilder.Entity<TicketResponse>().Property<int>("TicketFK");
            modelBuilder.Entity<TicketResponse>().HasOne(tr => tr.Ticket).WithMany(t => t.Responses)
                .HasForeignKey("TicketFK");

            //can't store user data to database
            /*
            string ADMIN_ID = Guid.NewGuid().ToString();
            string ROLE_ID = ADMIN_ID;

            var hasher = new PasswordHasher<User>();
            modelBuilder.Entity<User>().HasData(
                new User
                {
                    Id = ADMIN_ID,
                    UserName = "test",
                    NormalizedUserName = "test".ToUpper(),
                    Email = "uniek@gmail.com",
                    PasswordHash = hasher.HashPassword(null, "admin"),
                    //PasswordHash = "test",
                    SecurityStamp = string.Empty
                });


            modelBuilder.Entity<IdentityRole>().HasData(
                new IdentityRole
                {
                    Id = ROLE_ID,
                    Name = "Admin"
                });


            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>
                {
                    RoleId = ROLE_ID, 
                    UserId = ADMIN_ID
                });
        */
        }
    }
}