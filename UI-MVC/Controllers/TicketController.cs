using System.Collections.Generic;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.MVC.Models;

namespace SC.UI.MVC.Controllers
{
    [Authorize]
    public class TicketController : Controller
    {
        private ITicketManager mgr;
        private readonly UserManager<User> userManager;
        public readonly SignInManager<User> SignInManager;
        public TicketController(ITicketManager ticketManager, UserManager<User> userManager, SignInManager<User> SignInManager)
        {
            mgr = ticketManager;
            this.userManager = userManager;
            this.SignInManager = SignInManager;
        }

        // GET: /Ticket
        public IActionResult Index()
        {
            IEnumerable<Ticket> tickets = mgr.GetTickets();
            return View(tickets);
        }

        public IActionResult ShowOnlyUserTicket()
        {
            IEnumerable<Ticket> tickets = mgr.GetTicketOfUser(userManager.GetUserId(User));
            return View(tickets);
        }

        // GET: /Ticket/Details/<ticket_number>
        public IActionResult Details(int id)
        {
            Ticket ticket = mgr.GetTicket(id);

            if (ticket.Responses != null)
                ViewBag.Responses = ticket.Responses;
            else
                ViewBag.Responses = mgr.GetTicketResponses(ticket.TicketNumber);

            return View(ticket);
        }

        // GET: /Ticket/Edit/<ticket_number>
        public IActionResult Edit(int id)
        {
            Ticket ticket = mgr.GetTicket(id);
            return View(ticket);
        }

        // POST: /Ticket/Edit/<ticket_number>
        [HttpPost]
        public IActionResult Edit(int id, Ticket ticket)
        {
            if (!ModelState.IsValid)
                return View(ticket);

            mgr.ChangeTicket(ticket);
            return RedirectToAction("Details", new {id = ticket.TicketNumber});
        }

        // GET: /Ticket/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: /Ticket/Create
        [HttpPost]
        public IActionResult Create(CreateTicketViewModel createTicket)
        {
            User user = userManager.FindByNameAsync(User.Identity.Name).Result;
            Ticket newTicket = mgr.AddTicket(user, createTicket.Problem);
            return RedirectToAction("Details", new {id = newTicket.TicketNumber});
        }

        // GET: /Ticket/Delete/<ticket_number>
        public IActionResult Delete(int id)
        {
            Ticket ticket = mgr.GetTicket(id);
            return View(ticket);
        }

        // POST: /Ticket/Delete/<ticket_number>
        [HttpPost]
        public IActionResult DeleteConfirmed(int id)
        {
            mgr.RemoveTicket(id);
            return RedirectToAction("Index");
        }
    }
}