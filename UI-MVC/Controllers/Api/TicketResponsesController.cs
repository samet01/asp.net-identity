using System.Linq;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;
using SC.UI.MVC.Models;

namespace SC.UI.MVC.Controllers.Api
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    public class TicketResponsesController : ControllerBase
    {
        private ITicketManager mgr;

        public TicketResponsesController(ITicketManager ticketManager)
        {
            mgr = ticketManager;
        }

        // GET: api/TicketResponses?ticketNumber=5
        [HttpGet()]
        public IActionResult Get(int ticketNumber)
        {
            var responses = mgr.GetTicketResponses(ticketNumber);

            if (responses == null || !responses.Any())
                return NoContent(); //of: StatusCode(StatusCodes.Status204NoContent);

            return Ok(responses);
        }
        
        // POST: api/TicketResponses
        [HttpPost]
        public IActionResult Post(/*[FromBody]*/NewTicketResponseDTO response)
        {
            /*if (!ModelState.IsValid)
                return BadRequest(ModelState);*/ // Automatisch bij [ApiController]

            TicketResponse createdResponse = mgr.AddTicketResponse(response.TicketNumber
                , response.ResponseText, response.IsClientResponse);

            //return Created($"/api/TicketResponses?id={createdResponse.Id}", createdResponse); // -> serializing: circulaire referentie!!
            // -> Oplossing 'DTO'
            TicketResponseDTO responseData = new TicketResponseDTO()
            {
                Id = createdResponse.Id,
                Text = createdResponse.Text,
                Date = createdResponse.Date,
                IsClientResponse = createdResponse.IsClientResponse,
                TicketNumberOfTicket = createdResponse.Ticket.TicketNumber
            };
            return Created($"/api/TicketResponses?id={createdResponse.Id}", responseData);
        }
    }
}
