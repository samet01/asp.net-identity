using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SC.BL;
using SC.BL.Domain;

namespace SC.UI.MVC.Controllers.Api
{
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    [ApiController]
    [Route("api/[controller]")]
    public class TicketsController : ControllerBase
    {
        private ITicketManager mgr;

        public TicketsController(ITicketManager ticketManager)
        {
            mgr = ticketManager;
        }

        // PUT: api/Tickets/5/State/Closed
        [HttpPut("{id}/State/Closed")]
        public IActionResult PutTicketStateToClosed(int id)
        {
            Ticket ticket = mgr.GetTicket(id);

            if (ticket == null)
                return NotFound();

            ticket.State = TicketState.Closed;
            mgr.ChangeTicket(ticket);

            return NoContent();
        }
    }
}