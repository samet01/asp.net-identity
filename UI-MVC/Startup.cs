using System;
using System.Text;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using SC.BL;
using SC.BL.Domain;
using SC.DAL;
using SC.DAL.EF;

namespace SC.UI.MVC
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddDbContext<SupportCenterDbContext
                >(); // = services.AddDbContext<SupportCenterDbContext>(ServiceLifetime.Scoped);
            services.AddScoped<ITicketRepository, TicketRepository>();
            services.AddScoped<ITicketManager, TicketManager>();

            services.AddControllersWithViews();

            //AddIdentity registers the services
            services.AddIdentity<User, IdentityRole>(config =>
                {
                    config.Password.RequiredLength = 5;
                    config.Password.RequireUppercase = true;
                    config.Password.RequireNonAlphanumeric = false;
                })
                .AddEntityFrameworkStores<SupportCenterDbContext>()
                .AddDefaultTokenProviders();
            
            /*
            // it doesn't work -> I still can't log in  -> i learnt options within addauthentication will broke the cookie
            // jwt for api and cookie for mvc
            services.AddAuthentication(options => {
                    options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddCookie(options =>
                {
                    //options.LoginPath = "/Account/Login";
                    //options.AccessDeniedPath = "/Home/Error";
                    
                    //claims
                    options.Cookie.Name = "Identity.Cookie";
                    //bring the user to login page if he or she is not authorized
                    options.LoginPath = "/User/Login";
                })
                .AddJwtBearer(options =>
                {
                    options.SaveToken = true;
                    options.RequireHttpsMetadata = false;
                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidAudience = " you site link blah blah",
                        ValidIssuer = "You Site link Blah  blah",
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes("supersecretkey")),
                        ValidateLifetime = true,
                        ClockSkew = TimeSpan.Zero
                    };
                });
            */
            services.AddAuthentication()
                .AddGoogle(options =>
                {
                    options.ClientId = "430888065364-fh2vu86l7p65rsc20battjvtsmjjioa8.apps.googleusercontent.com";
                    options.ClientSecret = "5As38xyp_OzRdWaGEYAwV4O8";
                })
                .AddJwtBearer(options =>
                {
                    options.ClaimsIssuer = Configuration["Authentication:ClaimsIssuer"];
                    options.SaveToken = true;
                    options.Authority = Configuration["Authentication:Authority"];
                    options.Audience = Configuration["Authentication:Audience"];
                    options.RequireHttpsMetadata = false;

                    //I learnt this cannot be used anymore -> because of mvc5 upgrade
                    /*options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters()
                    {
                        ValidateIssuerSigningKey = true,

                        ValidateIssuer = true,
                        ValidIssuer = Configuration["Authentication:ValidIssuer"],
                        ValidateAudience = true,
                        ValidAudience = Configuration["Authentication:ValidAudience"],
                        ValidateLifetime = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Authentication:SecurityKey"]))
                    };*/
                });


            services.PostConfigure<CookieAuthenticationOptions>(IdentityConstants.ApplicationScheme,
                opt =>
                {
                    //configure your other properties
                    opt.Cookie.Name = "Identity.Cookie";
                    opt.LoginPath = "/User/Login";
                    opt.LogoutPath = "/User/LogOut";
                    opt.AccessDeniedPath = "/User/AccessDenied";
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            // who are you
            app.UseAuthentication();

            // are you allowed
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}