﻿/**
 * Ophalen antwoorden van een ticket
 */
const ticketNumberSpan = document.getElementById('ticketNumber');
const ticketNumber = Number(ticketNumberSpan.innerText);

const loadResponsesButton = document.getElementById('loadResponses');
loadResponsesButton.addEventListener('click', function () { loadResponsesOfTicket(ticketNumber); });

function loadResponsesOfTicket(ticketNumber) {
    fetch('/api/TicketResponses?ticketnumber='+ticketNumber, {
        method: 'GET',
        headers: { 'Accept': 'application/json' }
    })
        .then(function (response) { if (response.ok) return response.json(); })
        .then(function (data) { showTicketResponses(data); })
        .catch(function (error) { alert('Oeps, something went wrong!'); });
}
function showTicketResponses(responses) {
    const responsesTable = document.getElementById('responses');
    $(responsesTable).find('tr:gt(0)').remove();
    setTimeout(function () {
        responses.forEach(function (response) { addResponseToList(response); });
    }, 1000);
}
function addResponseToList(response) {
    const date = new Date(response.date);
    const checked = response.isClientResponse ? 'checked="checked"' : '';
    const responsesTable = document.getElementById('responses');
    $(responsesTable).append(`<tr>
                                <td>${response.text}</td>
                                <td>${date.toLocaleString(navigator.language)}</td>
                                <td><input type="checkbox" disabled ${checked}/></td>
                              </tr>`);
}

/**
 * Voeg een nieuw response toe
 */
const createResponseButton = document.getElementById('createResponse');
createResponseButton.addEventListener('click', function () { createResponse(); });

function createResponse() {
    const answer = document.getElementById('responseText').value;
    const newResponse = { 
        ticketNumber: ticketNumber, 
        responseText: answer,
        isClientResponse: false 
    };
    
    fetch('/api/TicketResponses', {
        method: 'POST',
        body: JSON.stringify(newResponse),
        headers : {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        }
    })
        .then(function (response) { if (response.ok) return response.json(); })
        .then(function (data) { 
            addResponseToList(data);
            document.getElementById('responseText').value = ''; 
        })
        .catch(function (error) { alert('Oeps, something went wrong!'); });
}

/**
 * Wijzig de status van het ticket naar 'Closed'
 */
const closeTicketButton = document.getElementById('changeStatusToClosed');
closeTicketButton.addEventListener('click', function () { closeTicket(ticketNumber); });

function closeTicket(ticketNumber) {
    fetch('/api/Tickets/' + ticketNumber + '/State/Closed', {
        method: 'PUT'
    })
        .then(function (response) { 
            if (response.ok)
                document.getElementById('state').innerText = 'Closed';
        })
        .catch(function () { alert('Oeps, something went wrong!'); });
}
