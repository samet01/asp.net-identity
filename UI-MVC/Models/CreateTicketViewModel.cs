namespace SC.UI.MVC.Models
{
    public class CreateTicketViewModel
    {
        public int AccId { get; set; }
        public string Problem { get; set; }
    }
}
