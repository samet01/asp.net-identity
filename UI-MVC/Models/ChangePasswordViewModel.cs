﻿using System.ComponentModel.DataAnnotations;

namespace SC.UI.MVC.Models
{
    public class ChangePasswordViewModel
    {
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "huidige paswoord")]
        public string CurrentPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "nieuwe paswoord")]
        public string NewPassword { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Bevestig nieuw paswoord")]
        [Compare("NewPassword", ErrorMessage = "nieuwe paswoord en bevestig nieuwe paswoord komen niet overeen")]
        public string ConfirmPassword { get; set; }
    }
}