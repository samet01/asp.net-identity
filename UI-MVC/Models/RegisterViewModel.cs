﻿using System.ComponentModel.DataAnnotations;
using SC.BL.Domain;

namespace SC.UI.MVC.Models
{
    public class RegisterViewModel
    {
        [Required] public string Email { get; set; }

        [Required] public string Username { get; set; }

        [Required]
        [Display(Name = "admin of gebruiker?")]
        public Role RoleUser { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "bevestig paswoord opnieuw")]
        [Compare("Password",
            ErrorMessage = "paswoord en bevestig nieuwe paswoord komen niet overeen")]
        public string ConfirmPassword { get; set; }
    }
}